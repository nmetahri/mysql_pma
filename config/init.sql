-- Attribution de tous les privilèges à l'utilisateur sur toutes les bases de données
GRANT ALL PRIVILEGES ON *.* TO 'database'@'%' WITH GRANT OPTION;

-- Met à jour les privilèges immédiatement après les modifications
FLUSH PRIVILEGES;