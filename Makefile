DC = docker-compose -f docker-compose.yml
DATABASE_CONTAINER = db
MYSQL_CONNECTION = mysql -h 0.0.0.0 -u database -pdatabase
MYSQL_CONNECTION_ROOT = mysql -h 0.0.0.0 -u root -proot

.PHONY: help logs

.DEFAULT_GOAL := help

help: ## Display help
	@echo "Available targets:"
	@awk '/^[a-zA-Z_-]+:/ { \
		helpMessage = helpMessage; \
		n = split($$0, a, "##"); \
		if (n == 2) { \
			helpMessage = helpMessage "  " substr($$1, 1, length($$1)-1) "\t\t" a[2] "\n"; \
		} \
	} END {printf "%s", helpMessage}' $(MAKEFILE_LIST)

up: ## Start the docker containers
	$(DC) up -d --build

do: ## Stop the docker containers
	$(DC) down

logs: ## Show the docker logs
	$(DC) logs -f

ex: ## Enter the db container
	$(DC) exec $(DATABASE_CONTAINER) bash

db: ## Enter the db container
	$(DC) exec $(DATABASE_CONTAINER) $(MYSQL_CONNECTION)

dba: ## Enter the db container as root
	$(DC) exec $(DATABASE_CONTAINER) $(MYSQL_CONNECTION_ROOT)

dbim: prep-args ## Import the database
	$(DC) exec -T $(DATABASE_CONTAINER) $(MYSQL_CONNECTION) < $(runargs)

prep-args: ## [Internal] Pre-recipe for commands with args
	@echo -e "\e[1;44m Please be aware that you cannot concatenate recipes with this command! \e[0m"
	$(eval runargs=$(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS)))